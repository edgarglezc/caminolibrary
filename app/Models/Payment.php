<?php

namespace App\Models;

use App\Http\Controllers\SupplierController;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

    protected $fillable = [
        'supplier_id',
        'total',
        'status',
        'date'
    ];

    public function suppliers()
    {
        return $this->hasMany(Supplier::class); 
    }
}
