<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionController extends Controller
{
    public function userLogin(Request $request)
    {
        dd($request);
        $data = $request->input();
        $request->session()->put('username', $data['username']);
        $request->session()->put('is_admin', $data['is_admin']);
        dd($data);
        return redirect('index');
    }
}
