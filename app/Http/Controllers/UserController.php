<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session()->has('username')){
            return view('user');
        }
        return view('login');
    }

    public function logout()
    {
        session()->forget('username');
        session()->forget('is_admin');
        session()->forget('id');
        return redirect()->route('index')->with('message', 'Has cerrado sesión');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->has('login')){
            $data = $request->input();
            $user = User::where('username', $data['username'])->first();
            $arr = (array)$user;
            if($arr) {
                if($user['password'] == $data['password']){
                    $request->session()->put('username', $user['username']);
                    $request->session()->put('id', $user['id']);
                    $request->session()->put('is_admin', $user['is_admin']);
                }
            }
            return redirect()->route('index')->with('message', 'Has iniciado sesión');
        }
        User::create($request->all());
        return redirect()->route('users.index')->with('message', 'Usuario creado exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
