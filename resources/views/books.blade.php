@extends('master')

@section('title')
Camino Librerías - Libros
@endsection

@section('content')
<div class="container">
    @extends('header')

    <center>
        <form action="" class="search-form">
            <label for="search">Buscar libro:</label>
            <input type="text" name="search" id="search" class="search-bar">
            <button type="submit" class="book-button">Buscar</button>
        </form>
        @if(session('is_admin') == 1)
            <a href=""><button class="add-button">Agregar</button></a>
            <a href=""><button class="delete-button">Eliminar</button></a>
        @endif
    </center>

    <article class="book">
        <a href="">
            <img src="libros/pedroparamo.png" alt="libro pedro paramo">
        </a>
        <div class="book-info">
            <p>Pedro Páramo</p>
            <p>Juan Rulfo</p>
            <p>$85.00</p>
            <button class="book-button">
                <img src="imagenes/cart.png" alt="cart" width="24px">
                Agregar al Carrito
            </button>
        </div>
    </article>

    <article class="book">
        <a href="">
            <img src="libros/personanormal.png" alt="libro pedro paramo">
        </a>
        <div class="book-info">
            <p>Persona Normal</p>
            <p>Benito Taibo</p>
            <p>$100.00</p>
            <button class="book-button">
                <img src="imagenes/cart.png" alt="cart" width="24px">
                Agregar al Carrito
            </button>
        </div>
    </article>

    <article class="book">
        <a href="">
            <img src="libros/batallasdesierto.png" alt="libro pedro paramo">
        </a>
        <div class="book-info">
            <p>Las Batallas en el Desierto</p>
            <p>José Emilio Pacheco</p>
            <p>$85.00</p>
            <a href="cart.html"><button class="book-button">
                    <img src="imagenes/cart.png" alt="cart" width="24px">
                    Agregar al Carrito
                </button></a>
        </div>
    </article>

    <article class="book">
        <a href="">
            <img src="libros/vientodistante.png" alt="libro pedro paramo">
        </a>
        <div class="book-info">
            <p>El Viento Distante</p>
            <p>José Emilio Pacheco</p>
            <p>$85.00</p>
            <a href="cart.html"><button class="book-button">
                    <img src="imagenes/cart.png" alt="cart" width="24px">
                    Agregar al Carrito
                </button></a>
        </div>
    </article>

    <article class="book">
        <a href="">
            <img src="libros/elprincipito.png" alt="libro el principito">
        </a>
        <div class="book-info">
            <p>El Principito</p>
            <p>Antoine De Saint-Exupéry</p>
            <p>$75.00</p>
            <a href="cart.html"><button class="book-button">
                    <img src="imagenes/cart.png" alt="cart" width="24px">
                    Agregar al Carrito
                </button></a>
        </div>
    </article>

    <article class="book">
        <a href="">
            <img src="libros/readyplayerone.png" alt="libro ready player one">
        </a>
        <div class="book-info">
            <p>Ready Player One</p>
            <p>Ernest Cline</p>
            <p>$200.00</p>
            <a href="cart.html"><button class="book-button">
                    <img src="imagenes/cart.png" alt="cart" width="24px">
                    Agregar al Carrito
                </button></a>
        </div>
    </article>
    <a href="#" class="enlace-inferior"><p>Ir arriba</p></a>
    @extends('footer')
</div>
@endsection
