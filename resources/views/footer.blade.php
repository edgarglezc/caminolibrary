@section('footer')
<footer class="inferior">
        <div class="left">
            <center>
                <a href="index.html">
                    <img src="{{ asset('imagenes/logo.png') }}" alt="logo librería camino">
                </a>
                <a href="" class="enlace-inferior">
                    <p>
                        Condiciones de uso
                    </p>
                </a>
                <a href="" class="enlace-inferior">
                    <p>
                        Aviso de Privacidad
                    </p>
                </a>
            </center>
        </div>
        <div class="right">
            <h4 class="seguir">
                Síguenos en nuestras redes sociales:
            </h4>
            <center>
                <a href=""><img src="{{ asset('imagenes/facebook.png') }}" alt="facebook"></a>
                <a href=""><img src="{{ asset('imagenes/twitter.png') }}" alt="twitter"></a>
                <a href=""><img src="{{ asset('imagenes/instagram.png') }}" alt="instagram"></a>
                <a href=""><img src="{{ asset('imagenes/youtube.png') }}" alt="youtube"></a>
            </center>
        </div>
    </footer>
    @endsection