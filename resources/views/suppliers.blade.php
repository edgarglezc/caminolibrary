@extends('master')

@section('title')
Camino Librerías - Proovedores
@endsection

@section('content')
<div class="container">
    @extends('header')

    <article class="supplier-history">
        <h4>Historial de compras a proveedores</h4>
        <table class="libros">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Proveedor</th>
                    <th>Fecha</th>
                    <th>Total</th>
                    <th>Estatus</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>Editorial Planeta México</td>
                    <td>01/10/2021</td>
                    <td>$5,000.00</td>
                    <td>
                        <p class="enviado">Pagado</p>
                    </td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Era</td>
                    <td>05/10/2021</td>
                    <td>$2,500.00</td>
                    <td>
                        <p class="pendiente">Pendiente</p>
                    </td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Porrúa</td>
                    <td>08/10/2021</td>
                    <td>$8,750.00</td>
                    <td>
                        <p class="enviado">Pagado</p>
                    </td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>Planeta</td>
                    <td>09/10/2021</td>
                    <td>$12,500.00</td>
                    <td>
                        <p class="enviado">Pagado</p>
                    </td>
                </tr>
                <tr>
                    <td>5</td>
                    <td>Era</td>
                    <td>11/10/2021</td>
                    <td>$9,000.00</td>
                    <td>
                        <p class="enviado">Pagado</p>
                    </td>
                </tr>
                <tr>
                    <td>6</td>
                    <td>Porrúa</td>
                    <td>16/10/2021</td>
                    <td>$15,000.00</td>
                    <td>
                        <p class="cancelado">Cancelado</p>
                    </td>
                </tr>
            </tbody>
        </table>
    </article>

    @extends('footer')
</div>
@endsection
