@extends('master')

@section('title')
Camino Librerías - Iniciar Sesión
@endsection

@section('content')
<div class="container">
    @extends('header')

    <center>
        <h3>Registra una cuenta</h3>
        <article class="login-box">
            <form method="POST" action="{{ route('users.store') }}" class="track-form">
                @csrf
                <label for="username">Nombre de usuario:</label>
                <input type="text" name="username" id="username" placeholder="Ingresa tu nombre de usuario">
                <label for="password">Contraseña:</label>
                <input type="password" name="password" id="password">
                <button type="submit">Crear cuenta</button>
            </form>
            <p>¿Ya tienes una cuenta? <a href="{{ route('users.index') }}">¡Inicia sesión!</a></p>
        </article>
    </center>
        
    @extends('footer')
</div>
@endsection
