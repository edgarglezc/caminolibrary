@extends('master')

@section('title')
Camino Librerías - Rastrear Pedido
@endsection

@section('content')
<div class="container">
    @extends('header')

    <section class="slider">
        <img src="imagenes/trackpage.jpg" alt="" width="100%" height="500px">
    </section>

    <center>
        <article class="track-box">
            <form action="" class="track-form">
                <label for="trackid">Rastrea tu pedido</label>
                <input type="text" name="trackid" id="trackid" placeholder="Ingresa el ID de tu pedido">
                <button type="submit">Rastrear</button>
            </form>
        </article>
    </center>

    @extends('footer')
</div>
@endsection
