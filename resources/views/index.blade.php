@extends('master')

@section('title')
Camino Librerías
@endsection

@section('content')
<div class="container">
    @extends('header')
    <section class="slider">
        <img src="imagenes/mainpage.jpg" alt="" width="100%" height="500px">
    </section>

    <section class="best-sellers">
        <h2>¡Estos son los mejores libros de la semana!</h2>
        <article class="book">
            <div class="book-info">
                <a href="">
                    <img src="libros/readyplayerone.png" alt="libro ready player one">
                </a>
                <p>Ready Player One</p>
                <p>Ernest Cline</p>
                <button class="book-button">
                    Ver Libro
                </button>
            </div>
        </article>
        <article class="book">
            <a href="">
                <img src="libros/batallasdesierto.png" alt="libro pedro paramo">
            </a>
            <div class="book-info">
                <p>Las Batallas en el Desierto</p>
                <p>José Emilio Pacheco</p>
                <button class="book-button">
                    Ver Libro
                </button>
            </div>
        </article>
        <article class="book">
            <a href="">
                <img src="libros/pedroparamo.png" alt="libro pedro paramo">
            </a>
            <div class="book-info">
                <p>Pedro Páramo</p>
                <p>Juan Rulfo</p>
                <button class="book-button">
                    Ver Libro
                </button>
            </div>
        </article>
    </section>

    <section class="cuerpo">
        <h1 class="titulo">
            De camino al conocimiento
        </h1>

        <article class="caja">
            <div class="subtitulo">
                <img src="imagenes/catalogue.jpg" alt="">
                <h3 class="servicios">Amplio Catálogo</h3>
            </div>
            <div class="contenido">
                Tenemos el catálogo más amplio del mercado.
                Encuentra tu libro favorito,
                desde literatura clásica hasta libros académicos.
                !Tenemos el libro indicado para ti!.
            </div>
        </article>

        <article class="caja">
            <div class="subtitulo">
                <img src="imagenes/delivery.jpg" alt="">
                <h3 class="servicios">Entregas Rápidas</h3>
            </div>
            <div class="contenido">
                Realiza tu pedido y recíbelo al día siguiente,
                monitoreando en todo en momento el estatus de tu orden.
            </div>
        </article>

        <article class="caja">
            <div class="subtitulo">
                <img src="imagenes/support.jpg" alt="">
                <h3 class="servicios">Soporte al Cliente</h3>
            </div>
            <div class="contenido">
                Si tienes cualquier duda respecto a nuestra tienda
                o con tu pedido, no dudes en comunicarte con
                nuestro soporte al cliente.
            </div>
        </article>
    </section>
</div>
@extends('footer')
</body>
@endsection