@section('header')
<header class="superior">
    <div class="logo">
        <img src="{{ asset('imagenes/logo.png') }}" alt="logo librería camino">
    </div>
    <nav class="menu">
        <ul class="opciones">
            <li><a href="{{ route('index') }}">INICIO</a></li>
            <li><a href="{{ route('about') }}">NOSOTROS</a></li>
            <li><a href="{{ url('/books') }}">LIBROS</a></li>
            <li><a href="{{ route('track') }}">RASTREAR PEDIDO</a></li>
            @if(session('is_admin') == 1)
            <li><a href="{{ url('/suppliers') }}"><img src="{{ asset('imagenes/truck.png') }}" alt="supplier icon"></a></li>
            @endif
            @if(session()->has('is_admin'))
            <li><a href="{{ route('cart') }}"><img src="{{ asset('imagenes/cart.png') }}" alt="cart icon"></a></li>
            @endif
            <li><a href="{{ route('users.index') }}"><img src="{{ asset('imagenes/user.png') }}" alt="user icon"></a></li>
        </ul>
    </nav>
</header>
@endsection