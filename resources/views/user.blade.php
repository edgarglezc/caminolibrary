@extends('master')

@section('title')
Camino Librerías - Perfil
@endsection

@section('content')
<div class="container">
    @extends('header')

    <article class="user-history">
        <a href="{{ route('users.logout') }}"><h1 class="enlace-inferior">Salir</h1></a>
        <h4>Historial de compras del usuario</h4>
        <table class="libros">
            <thead>
                <tr>
                    <th>#</th>
                    <th>ID Pedido</th>
                    <th>Estatus</th>
                    <th>Total</th>
                    <th>Fecha</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>CL16485</td>
                    <td>
                        <p class="pendiente">Pendiente</p>
                    </td>
                    <td>$380.00</td>
                    <td>Hoy</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>CL91548</td>
                    <td>
                        <p class="pendiente">Pendiente</p>
                    </td>
                    <td>$475.00</td>
                    <td>Ayer</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>CL98742</td>
                    <td>
                        <p class="enviado">Enviado</p>
                    </td>
                    <td>$170.00</td>
                    <td>08/10/2021</td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>CL34815</td>
                    <td>
                        <p class="cancelado">Cancelado</p>
                    </td>
                    <td>$275.00</td>
                    <td>15/09/2021</td>
                </tr>
            </tbody>
        </table>
    </article>

    <article>
        <h4>Basado en tus compras, te recomendamos...</h4>
        <article class="book">
            <div class="book-info">
                <a href="">
                    <img src="libros/readyplayerone.png" alt="libro ready player one">
                </a>
                <p>Ready Player One</p>
                <p>Ernest Cline</p>
                <button class="book-button">
                    Ver Libro
                </button>
            </div>
        </article>
        <article class="book">
            <a href="">
                <img src="libros/batallasdesierto.png" alt="libro pedro paramo">
            </a>
            <div class="book-info">
                <p>Las Batallas en el Desierto</p>
                <p>José Emilio Pacheco</p>
                <button class="book-button">
                    Ver Libro
                </button>
            </div>
        </article>
        <article class="book">
            <a href="">
                <img src="libros/pedroparamo.png" alt="libro pedro paramo">
            </a>
            <div class="book-info">
                <p>Pedro Páramo</p>
                <p>Juan Rulfo</p>
                <button class="book-button">
                    Ver Libro
                </button>
            </div>
        </article>
        <p>¡Explora los títulos recomendados para ti!</p>
    </article>
    @extends('footer')
</div>
@endsection
