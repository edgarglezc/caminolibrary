@extends('master')

@section('title')
Camino Librerías - Nosotros
@endsection

@section('content')
<div class="container">
    @extends('header')
    <section class="slider">
        <img src="imagenes/aboutpage.jpg" alt="" width="100%" height="500px">
    </section>

    <section class="cuerpo">
        <h1 class="titulo">
            Sobre nosotros
        </h1>
        <p>
            Librerías Camino es una tienda de libros mexicana que cuenta con más de 50 sucursales
            en todo el país, estamos presentes en los estados más importantes de la república como
            Guadalajara, Monterrey, Aguascalientes y Ciudad de México. Brindamos servicio nacional
            las 24 horas del día los 7 días de la semana a través de nuestra tienda en línea.
            Somos la mejor tienda de libros en el país, otorgando seguridad y rapidez en la compra
            de tus libros favoritos.
        </p>

        <article class="caja-mision">
            <div class="subtitulo">
                <h3 class="servicios">Misión</h3>
            </div>
            <div class="contenido">
                Difundir la cultura y el entretenimiento
                otorgando lugares seguros y dignos para
                la distribución y venta de libros.
            </div>
        </article>

        <article class="caja-vision">
            <div class="subtitulo">
                <h3 class="servicios">Visión</h3>
            </div>
            <div class="contenido">
                Ser una empresa referente en el mercado para el que
                estamos trabajando y para las personas.
            </div>
        </article>
    </section>
    @extends('footer')
</div>
@endsection
