<?php

use App\Http\Controllers\BookController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\SupplierController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\SessionController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('index');

Route::get('/about', function () {
    return view('about');
})->name('about');

Route::get('/track', function () {
    return view('track');
})->name('track');

Route::get('/cart', function () {
    return view('cart');
})->name('cart');

Route::get('/login', function () {
    return view('login');
})->name('login');

Route::get('/register', function () {
    return view('register');
})->name('register');

Route::resources([
    'users' => UserController::class,
    'books' => BookController::class,
    'orders' => OrderController::class,
    'suppliers' => SupplierController::class,
    'payments' => PaymentController::class,
]);

Route::get('user', [UserController::class, 'logout'])->name('users.logout');